#pragma once
#include <SDL2/SDL.h>
#include <utility>

namespace playerConsts{
  const int NONE  = 0x0;
  const int UP    = 0x1;
  const int DOWN  = 0x2;
  const int LEFT  = 0x4;
  const int RIGHT = 0x8;
}

class Entity 
{
protected:
  SDL_Rect* r;
  int x, y;
public:
  virtual void handle_event(SDL_Event) = 0;
  virtual void render(SDL_Renderer*) = 0;
  virtual void tick() = 0;
  ~Entity() {};
  std::pair<int, int> getPos() {return std::make_pair((int)x, (int)y);}
  SDL_Rect* get_SDL_Rect();
};
