#include <iostream>
#include <SDL2/SDL.h>
#include "player.h"
#include "state.h"

#define WINDOW_X 800
#define WINDOW_Y 600

void handleEvent(bool *loop, GameState s) {
  SDL_Event event;
  while (SDL_PollEvent(&event))
  {
    for (Entity* e : s.getEntities())
      e->handle_event(event);
    switch (event.type)
    {
      case SDL_QUIT:
        *loop = false;
        break;
      default:
        continue;
    }
  }
}

void renderFrames(SDL_Renderer* ren, GameState s)
{
  bool loop = true;
  while (loop) 
  {
    handleEvent(&loop, s);
    s.tick();
    SDL_SetRenderDrawColor(ren, 0xf0, 0xff, 0xff, 0xff);
    SDL_RenderClear(ren);
    for (Entity* e : s.getRenderableEntities())
      e->render(ren);
    SDL_RenderPresent(ren);
  }
}

void handleError(std::string message)
{
  std::cout << message << SDL_GetError() << std::endl;
}

int main(int argc, char** argv)
{
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
  {
    handleError("Failed to initialize SDL video: ");
    return -1;
  }

  SDL_Window* mainWin = SDL_CreateWindow(
    "Test window",
    0, 0,         //(x, y) pos
    WINDOW_X, WINDOW_Y,
    SDL_WINDOW_SHOWN);

  if (mainWin == nullptr)
  {
    handleError("Failed to create main window: ");
    return -1;
  }

  SDL_Renderer* mainRen = SDL_CreateRenderer(
    mainWin,
    -1,     // Use any driver.
    SDL_RENDERER_ACCELERATED);

  if (mainRen == nullptr)
  {
    handleError("Failed to create main renderer: ");
    return -1;
  }

  SDL_RenderSetLogicalSize(mainRen, WINDOW_X, WINDOW_Y);
  GameState* s = new GameState(WINDOW_X, WINDOW_Y);

  renderFrames(mainRen, *s);
}
