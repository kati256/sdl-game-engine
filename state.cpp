#include "state.h"
#include "magic.h"
#include <unistd.h>

void removeEntity(Entity* e)
{
}

GameState::GameState(int w, int h) 
{
    worldX = worldY = 0;
    screenW = w;
    screenH = h;
    mainPlayer = new Player(50, 50);
    tickrate = 1000 / defaultTicksPerSecond;
    entities.push_back(mainPlayer);
}

GameState::~GameState() 
{
    std::for_each (entities.begin(), entities.end(), removeEntity);
}

Player GameState::getPlayer()
{
    return *mainPlayer;
}

void GameState::tick() {
    for (Entity* e : entities)
        e->tick();
    usleep(tickrate);
}

bool GameState::isBounded(std::pair<int, int> pos) 
{
    return pos.first/sizeDivider >= worldX &&
        pos.first  /sizeDivider  < worldX + screenW &&
        pos.second /sizeDivider  >= worldY &&
        pos.second /sizeDivider  < worldY + screenH;
}

std::vector<Entity*> GameState::getRenderableEntities() 
{
    std::vector<Entity*> res;
    for (Entity* e : entities)
        if (isBounded(e->getPos()))
            res.push_back(e);
   return res;
}

std::vector<Entity*> GameState::getEntities()
{
    return entities;
}