#pragma once
#include <vector>
#include "player.h"

class GameState
{
public:
    GameState(int w, int h);
    ~GameState();

    void tick();
    std::vector<Entity*> getRenderableEntities();
    std::vector<Entity*> getEntities();
    Player getPlayer();
private:
    bool isBounded(std::pair<int, int>);
    int worldX, worldY, screenW, screenH;
    int tickrate;
    std::vector<Entity*> entities;
    Player* mainPlayer;
};