INCFLAGS=-I/usr/local/include
CPPFLAGS=-g -std=c++11 -Wall
LDFLAGS=-g -L/usr/local/lib
LDLIBS=-lSDL2

SRCS=main.cpp player.cpp state.cpp entity.cpp geo/circle.cpp
OBJS=$(subst .cpp,.o,$(SRCS))

all: main

main: $(OBJS)
	$(CXX) $(LDFLAGS) -o main-d $(OBJS) $(LDLIBS) 

obj\\%.o: %.cpp
	$(CXX) $(CPPFLAGS) -c $< -o $@

clean:
	rm -rd *.o main-d *.dSYM
