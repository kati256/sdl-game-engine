#pragma once
#include <utility>
#include <SDL2/SDL.h>

typedef std::pair<int, int> Point;

class Circle 
{
public:
    Point centre;
    int radius;
    SDL_Color color;
    Circle(Point c, int r);
    void render(SDL_Renderer*);
};