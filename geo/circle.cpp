#include "circle.h"

Circle::Circle(Point c, int r) 
{
    centre = c;
    radius = r;
    color.r = 0xff;
    color.g = 0x00;
    color.b = 0x00;
    color.a = 0xff;
}

void Circle::render(SDL_Renderer* ren)
{
    SDL_SetRenderDrawColor(ren, color.r, color.g, color.b, color.a);
    for (int w = 0; w < radius * 2; w++)
    {
        for (int h = 0; h < radius * 2; h++)
        {
            int dx = radius - w; // horizontal offset
            int dy = radius - h; // vertical offset
            if ((dx*dx + dy*dy) <= (radius * radius))
            {
                SDL_RenderDrawPoint(ren, centre.first + dx, centre.second + dy);
            }
        }
    }
}