#include "player.h"
#include "magic.h"

Player::Player(int w, int h) : hitbox(*new Circle(std::make_pair(w/2, h/2), 5))
{
  r = new SDL_Rect;
  x = 0;
  y = 0;
  pl_speed = 1;
  direction = playerConsts::NONE;

  r->x = 0;
  r->y = 0;
  r->w = w;
  r->h = h;
}

Player::~Player()
{
  // Do nothing for now
}

void Player::render(SDL_Renderer* ren) 
{
    SDL_SetRenderDrawColor(ren, 0xfa, 0xad, 0xa5, 0xff);
    SDL_RenderDrawRect(ren, r);
    hitbox.render(ren);
}

void Player::handle_event(SDL_Event event)
{
  if (event.type == SDL_KEYDOWN)
  {
    switch (event.key.keysym.sym)
    {
        case SDLK_DOWN:
          direction |= playerConsts::DOWN;
          break;
        case SDLK_UP:
          direction |= playerConsts::UP;
          break;
        case SDLK_LEFT:
          direction |= playerConsts::LEFT;
          break;
        case SDLK_RIGHT:
          direction |= playerConsts::RIGHT;
          break;
        default:
          break;
    }
  } 
  else if (event.type == SDL_KEYUP)
  {
    switch (event.key.keysym.sym)
    {
        case SDLK_DOWN:
          direction &= ~playerConsts::DOWN;
          break;
        case SDLK_UP:
          direction &= ~playerConsts::UP;
          break;
        case SDLK_LEFT:
          direction &= ~playerConsts::LEFT;
          break;
        case SDLK_RIGHT:
          direction &= ~playerConsts::RIGHT;
          break;
        default:
          break;
    }
  }
}

void Player::tick() 
{
  if (direction & playerConsts::DOWN)
    down();
  if (direction & playerConsts::UP)
    up();
  if (direction & playerConsts::LEFT)
    left();
  if (direction & playerConsts::RIGHT)
    right();
}

void Player::updateHitbox()
{
  hitbox.centre.first  = (x/sizeDivider + r->w/2);
  hitbox.centre.second = (y/sizeDivider + r->h/2);
}

void Player::updateRenderPosition()
{
  r->y = y/sizeDivider;
  r->x = x/sizeDivider;
}

void Player::up()
{
  y -= pl_speed;
  updateRenderPosition();
  updateHitbox();
}

void Player::down()
{
  y += pl_speed;
  updateRenderPosition();
  updateHitbox();
}

void Player::left()
{
  x -= pl_speed;
  updateRenderPosition();
  updateHitbox();
}

void Player::right()
{
  x += pl_speed;
  updateRenderPosition();
  updateHitbox();
}
