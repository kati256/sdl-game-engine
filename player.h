#pragma once
#include "entity.h"
#include "geo/circle.h"
#include <SDL2/SDL.h>
#include <utility>

class Player : public Entity
{
private:
  int pl_speed;
  int direction;
  Circle hitbox;
  void up();
  void down();
  void left();
  void right();
  void updateHitbox();
  void updateRenderPosition();

public:
  Player(int, int);
  ~Player();
  void handle_event(SDL_Event) override;
  void tick() override;
  void render(SDL_Renderer*) override;
};
